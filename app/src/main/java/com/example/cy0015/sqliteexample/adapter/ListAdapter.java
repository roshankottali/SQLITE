package com.example.cy0015.sqliteexample.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.cy0015.sqliteexample.R;
import com.example.cy0015.sqliteexample.database.model.Note;

import java.util.ArrayList;
import java.util.List;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.MyViewHolder> {
    private List<Note> aLlNote=new ArrayList<>();
    public ListAdapter(List<Note> aLlNote) {
        this.aLlNote=aLlNote;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_item,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.note_text.setText(aLlNote.get(position).getNote());

    }

    @Override
    public int getItemCount() {
        return aLlNote.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView note_text;
        public MyViewHolder(View itemView) {
            super(itemView);
            note_text=itemView.findViewById(R.id.note_text);
        }
    }
}
