package com.example.cy0015.sqliteexample;

import android.app.Dialog;
import android.content.DialogInterface;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.cy0015.sqliteexample.adapter.ListAdapter;
import com.example.cy0015.sqliteexample.database.DatabaseHelper;
import com.example.cy0015.sqliteexample.database.model.Note;
import com.example.cy0015.sqliteexample.utils.ClickListener;
import com.example.cy0015.sqliteexample.utils.RecyclerTouchListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ClickListener {
    LinearLayoutManager linearLayoutManager;
    ListAdapter listAdapter;
    RecyclerView recyclerView;
    private FloatingActionButton fab;
    private Dialog dialog;
    DatabaseHelper databaseHelper;
    List<Note> noteList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeView();

        databaseHelper = new DatabaseHelper(this);
        Log.d("ADDEDNOTEID", ">> " + databaseHelper.getALlNote().size());
        noteList.addAll(databaseHelper.getALlNote());


        listAdapter = new ListAdapter(noteList);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(listAdapter);
        /*recyclerView.setHasFixedSize(true);*/

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new Dialog(MainActivity.this);
                dialog.setContentView(R.layout.create_note_dialog);
                Button cancel = dialog.findViewById(R.id.cancel_note);
                Button save = dialog.findViewById(R.id.save_note);
                final EditText add_note_edit_text = dialog.findViewById(R.id.add_note_edit_text);
                dialog.show();
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Long id = databaseHelper.insert(add_note_edit_text.getText().toString().trim());

                        Note note = databaseHelper.getNote(id);

                        if (note != null) {
                            noteList.add(0, note);

                            listAdapter.notifyDataSetChanged();
                            Log.d("ADDEDNOTEID", ">> " + id);

                        }

                        dialog.dismiss();
                    }
                });
            }
        });


        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerView, MainActivity.this));

    }

    private void initializeView() {
        recyclerView = findViewById(R.id.recyclerview);
        fab = findViewById(R.id.fab);
    }

    @Override
    public void onClick(View view, int position) {

    }

    @Override
    public void onLongClick(View view, int position) {
        Toast.makeText(this, "CLICKED " + position, Toast.LENGTH_SHORT).show();
        showClickDialog(position);
    }

    private void showClickDialog(final int position) {

        CharSequence items[] = new CharSequence[]{"Edit", "Delete"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose option");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (which == 0) {
                    editFun(position);
                } else {
                    deleteFun(position);
                }
            }
        });
        builder.show();
    }

    private void editFun(final int position) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.edit_note_dialog);
        Button update = dialog.findViewById(R.id.edit_save);
        Button cancel = dialog.findViewById(R.id.edit_cancel);
        final EditText edit_note = dialog.findViewById(R.id.edit_note);

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!TextUtils.isEmpty(edit_note.getText().toString().trim())) {
                    updateNoteFun(edit_note.getText().toString().trim(), position);
                    dialog.dismiss();
                }


            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void updateNoteFun(String n, int position) {
        Note note = noteList.get(position);
        note.setNote(n);

        databaseHelper.noteUpdate(note);

        noteList.set(position, note);
        listAdapter.notifyItemChanged(position);
    }

    private void deleteFun(int position) {
        databaseHelper.deleteNote(noteList.get(position));
        noteList.remove(position);
        listAdapter.notifyItemRemoved(position);
    }
}
