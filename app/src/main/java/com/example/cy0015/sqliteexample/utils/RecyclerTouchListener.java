package com.example.cy0015.sqliteexample.utils;

import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import com.example.cy0015.sqliteexample.MainActivity;

public class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

    private GestureDetector gestureDetector;
    MainActivity context;

    public RecyclerTouchListener(final MainActivity mainActivity, final RecyclerView recyclerView, MainActivity mainactivty) {

        this.context=mainactivty;
        gestureDetector=new GestureDetector(mainActivity,new GestureDetector.SimpleOnGestureListener(){
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }

            @Override
            public void onLongPress(MotionEvent e) {
                View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                if (child != null && context != null) {
                    context.onLongClick(child,recyclerView.getChildAdapterPosition(child));
                }

            }
        });
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

        View child = rv.findChildViewUnder(e.getX(), e.getY());
        if (child != null && context != null && gestureDetector.onTouchEvent(e)) {
            context.onLongClick(child,rv.getChildAdapterPosition(child));
        }

        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }
}
