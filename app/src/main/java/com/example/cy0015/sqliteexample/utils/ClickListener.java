package com.example.cy0015.sqliteexample.utils;

import android.view.View;

public interface ClickListener {

    void onClick(View view, int position);

    void onLongClick(View view, int position);
}
